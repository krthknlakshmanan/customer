### `Create a purchase details portal based on the customer`:
 ```bash
 
1. Create an API to upload the product information into the database. All products should be created via upload only.

2. If product info already exist in the database , should update it else insert the product

3. Api for Basic login and registration setup

4. JWT token should be properly handled for all the API.

4. Create an API to create order, update order, cancel order.

5. Create an api to list ordered products based on the customer. (Should include search and sort functionality)

6. Api to list ordered product count based on date.

7. Api to list customers based on the number of products purchased.

 ```
 > Note: Use the package manager [`composer`](https://getcomposer.org/download/) to install composer.
 
## `Usage`

```sh
1. cd Customer/
2. npm install -y
3. npm run serve
```

## `Testing`
Verify the deployment by navigating to your server address in
your preferred browser.

```sh
http://127.0.0.1:3080
```
#### `README.md - Design fork - ` [`Dillinger`](https://dillinger.io)